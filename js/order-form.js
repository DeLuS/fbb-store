/**
 *  При клике на кнопку "Купить" на странице книги открывается
 форма оформления заказа
*/	

$(document).on('click', function (event){
    if (event.target.tagName == 'INPUT') {
        var newWin = window.open("order.html", 'Оформить заказ');
        $.getJSON('https://netology-fbb-store-api.herokuapp.com/book/'+event.target.getAttribute('id'), function(book) {
            body = newWin.document.body;
            newWin.$('#order').html('Оформить заказ на книгу &laquo;' + book.title + '&raquo;')
            newWin.$('#orderImg').append('<img src="' + book.cover.small + '" alt="' + book.title + '">');
            var div = newWin.document.createElement('div'),
            body = newWin.document.body;
            div.id = book.id; 
            div.className='bookDescription';
            div.innerHTML = '';
            body.appendChild(div);
            newWin.$('#total-payment').attr('data-price', book.price);
            newWin.$('#total-payment').html('Итого к оплате: ' + book.price + ' Z');
        })
    }
})

