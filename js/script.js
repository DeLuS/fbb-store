/**
 *  Список книг загружается с сервера и отображается
*/

var booksArray=[];
var curNumberBooks=0;
var numberInRow=4;

$(document).on('ready', function () {
    $.getJSON('https://netology-fbb-store-api.herokuapp.com/book/', function(json){
        $.each(json, function(index, element){
     	    booksArray.push(element);
         })
        printRowBooks(curNumberBooks, numberInRow);
        $('#preloader').hide();
    });
})

function printRowBooks() {
    var rest = booksArray.length - curNumberBooks;
    var imax = rest < numberInRow ? rest : numberInRow;
    $('#books').append('<div class="row">');
    for (var i = 0; i < imax; i++){
        $('#books .row:last').append('<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 book"><img src="'+
        booksArray[curNumberBooks].cover.small+'" class="'+booksArray[curNumberBooks].id+'" alt="'+booksArray[curNumberBooks].title+'"><p>' + booksArray[curNumberBooks].info + '</p></div>');
        curNumberBooks++;
    }
    $('#books').append('</div>');
    if (rest < numberInRow) {
        $('#show-more').hide();
    }
 
}

/**
 *  По нажатию кнопки на главной странице появляются ещё 4 книги 
*/	

$( '#show-more' ).on('click', function (event){
    printRowBooks(curNumberBooks, numberInRow);
});


/**
 *  На главной странице при клике на книгу открывается страница книги 
*/	

var newWin;
   
$(document).on('click', function (event){
    var target = event.target;
    if (target.tagName == 'IMG') {
         newWin = window.open("book.html", 'Страница книги');
         $.getJSON('https://netology-fbb-store-api.herokuapp.com/book/'+target.getAttribute('class'), function(book) {
            var div = newWin.document.createElement('div');
            body = newWin.document.body;
            div.id = book.id; 
            div.className='bookDescription col-lg-4 col-md-4 col-sm-12 col-xs-12';
            div.innerHTML = '<img src="'+book.cover.large+'" class="fbbBook img-responsive center-block" alt="'+book.title+'"><div class="eye"></div><div class="eyeball"></div><p class="bookText">'+ book.description+'</p>';
            var divBook = body.querySelector('#book');
            divBook.appendChild(div);
            var newButton = newWin.document.createElement('input');
            newButton.type= 'button';
            newButton.id = book.id;
            newButton.className='buy btn btn-primary btn-block';
            newButton.value='Купить за жалкие ' + book.price +' Z';
            div.appendChild(newButton);
            var divReviews = newWin.document.createElement('div');
            divReviews.className='col-lg-4 col-md-4 col-sm-12 col-xs-12';
            divBook.insertBefore(divReviews, div);
            book.reviews.forEach(function(element) {
                var newP = newWin.document.createElement('p');
                newP.innerHTML='<img src="'+ element.author.pic +'" class="reviewsBook" alt="'+element.author.name+'">' +
                      '<blockquote>' + element.cite + '</blockquote>';
                divReviews.appendChild(newP);
            })
            var divFeatures = newWin.document.createElement('div');
            divFeatures.className='col-lg-4 col-md-4 col-sm-12 col-xs-12';
            divBook.appendChild(divFeatures);
            book.features.forEach(function(element) {
                var newP = newWin.document.createElement('p');
                newP.innerHTML='<img src="'+ element.pic +'" class="reviewsBook" alt="'+element.title+'">' +
                      '<blockquote>' + element.title + '</blockquote>';
                divFeatures.appendChild(newP);
            })
            newWin.$('#preloader').hide();
        })
      }
})

