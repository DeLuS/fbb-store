var btnSubmit = document.querySelector('#toOrder');
var preloader = document.querySelector('#preloader');
var formOrder = document.querySelector('[data-role="json-request-form"]');
var xhr;
var divError = document.querySelector('[data-role="json-request-error"]');
var firstName = document.querySelector('#firstName');
var phone = document.querySelector('#phone');
var email = document.querySelector('#email');

function loadAsync(url) {
  xhr = new XMLHttpRequest();
  xhr.open("POST", url, true);
  xhr.addEventListener('load', onLoadAsync);
  xhr.addEventListener('error', onLoadError);
  xhr.setRequestHeader('Content-Type', 'application/json');
  var body = {
        "manager": "manager@fbb-store.com",
        "book": "fbb-0002",
        "name": "Ivanov Ivan",
        "phone": "+99934509834",
        "email": "ivan@mail.ru",
        "comment": "It`s a present",
        "delivery": {
            "id": "delivery-05",
            "address": "Earth, Russia, Moscow city, Kremlin"
        },
        "payment": {
            "id": "payment-02",
            "currency": "R01589"
        }
    }
  xhr.send(body);
  return xhr;
}

function onLoadAsync() {
  preloader.style.display = 'none';
  if (xhr.status != 200) {
    divError.innerHTML = "<strong>Ошибка при отправке заказа:</strong>" + xhr.statusText;
  } 
    else{
    divError.innerHTML = "Оформление заказа прошло успешно"; 
  }
  divError.style.display = 'block';     
}

function onLoadError() {
  divError.innerHTML = "Ошибка. Что-то пошло не так"; 
  divError.style.display = 'block';
  divError.classList.add('alert');
  divError.classList.add('alert-danger');        
  preloader.style.display = 'none';     
}

/**
 *  Осуществляется проверка введенных пользователем данных. 
 *  В случае успешной проверки отправка данных на сервер 
 *  
*/
btnSubmit.onclick = function () {
  var reg = /^[a-zA-Z\s]{1,20}$/g;
  if (! reg.test(firstName.value)) {
      alert('Ошибка. Проверьте, пожалуйста, поле "Имя". Имя может содержать не более 20 латинских символов');
  } else {
  var reg = /^\+(\d){11}$/g;
  if (! reg.test(phone.value)) {
      alert('Ошибка. Проверьте, пожалуйста, поле "Телефон". Телефон должен быть в формате +99934509834');
   } else {   
  var reg = /^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/g;
  if (! reg.test(email.value)) {
      alert('Ошибка. Проверьте, пожалуйста, поле "Эл.адрес"');
   } else {          
      event.preventDefault();
      loadAsync('https://netology-fbb-store-api.herokuapp.com/order'); 
      preloader.style.display = 'block';
  }
}}}; 


/**
 *  При изменении способа доставки пересчитать общую сумму заказа
*/
$('input[name=delivery]:radio').on('change', function() {
     var price = Number($('#total-payment').attr('data-price'));
     price += Number(this.value);
     $('#total-payment').html('Итого к оплате: ' + price + ' Z');  
})        